require 'net/http'

def download_spec(name)
    puts "downloading " + name + "..."
    Net::HTTP.start("www.opengl.org") do |http|
        resp = http.get("/registry/api/" + name)
        File.open(name, "wb") do |file|
            file.write(resp.body)
        end
    end
    puts "done"
end

class GLtypemap
    attr_accessor :types

    def initialize
        @types = Hash.new("")
    end
    
    def parse
        file = File.open("./gl.tm").read
        file.gsub!(/\r\n?/, "\n")

        file.each_line do |line|
            if line[0] != '#'
                ln = line.split(',')
                @types[ln[0]] = ln[3].strip if ln[0] != "void"
            end
        end
    end
end

class GLfunction
    attr_accessor :ret_type
    
    def initialize(name)
        @ret_type = "void"
        @name = name
        @params = []
    end
    
    def add_param(type, name)
        @params += [type + " " + name]
    end
    
    def definition
        return "%{r} gl%{n}(%{p});" % {:r => @ret_type, :n => @name, :p => @params.join(", ")}
    end
end

class Enum
    attr_accessor :name
    
    def initialize(name)
        @name = name
        @depends_on = []
        @gl_consts = []
        @gl_functions = []
        
        @longest_value_len = 0
    end
    
    def add_dep(dep_name)
        @depends_on += [dep_name] if @depends_on.index(dep_name).nil?
    end
    
    def add_const(name, value)
        @gl_consts += [{:name => name, :value => value}]
        @longest_value_len = name.length if @longest_value_len < name.length
    end
    
    def add_function(func)
        @gl_functions += [func]
    end
    
    def save(dir)
        File.open(dir + "/" + @name.downcase + ".h", :mode => "wt+") do |f|
            h = "GL_" + @name.upcase
            
            f.puts "#ifndef %{h}" % {:h => h}
            f.puts "#define %{h} 1" % {:h => h}
            f.puts 
            
            f.puts "#ifdef __cplusplus"
            f.puts "extern \"C\" {"
            f.puts "#endif"
            f.puts
            
            f.puts %-#include "gl_types.h"-
            f.puts
            
            if @depends_on.count > 0
                @depends_on.each do |dep|
                    f.puts %-#include "%{dep}.h"- % {:dep => dep.downcase}
                end            
                f.puts
            end
            
            if @gl_consts.count > 0
                mask = "#define GL_%%%{n}s %%s" % {:n => -@longest_value_len}
                @gl_consts.each do |v|                   
                    #f.puts "#define GL_%{n} %{v}" % {:n => v[:name], :v => v[:value]}
                    f.puts mask % [v[:name], v[:value]]
                end            
                f.puts
            end
            
            if @gl_functions.count > 0
                @gl_functions.each { |gl_func| f.puts gl_func.definition }
                f.puts
            end
            
            f.puts "#ifdef __cplusplus"
            f.puts "}"
            f.puts "#endif"
            f.puts 
            f.puts "#endif // %{h}" % {:h => h}
        end        
    end
end

class EnumExtSpec 
    attr_accessor :enums, :vendors, :enum
    
    def initialize
        @enums = []
        @vendors = []
    end
    
    def parse_enum(words)
        if words[1] == 'enum:'            
            @enum = Enum.new(words[0])
            
            @enums += [@enum]
            v = words[0].split("_")[0]
            @vendors += [v] if @vendors.index(v).nil?
            
            return true
        else
            return false
        end
    end

    def parse_passthru(words)
        return (words[0] == 'passthru:')
    end
    
    def parse_value(words)
        if words[0] != 'profile:'
            @enum.add_const(words[0], words[2]) if !@enum.nil?
        end
        return true
    end
    
    def parse_dependence(words)
        if words[0] == 'use'
            @enum.add_dep(words[1])
            return true
        else
            return false
        end
    end
    
    def parse_enums
        file = File.open('./enumext.spec').read
        file.gsub!(/\r\n?/, "\n")

        l = 0
        
        file.each_line do |line|
            line.strip!
            
            if line[0] != '#' and !line.empty?
                l += 1 
                ln = line.split(" ")
                
                if !parse_enum(ln) 
                    if !parse_passthru(ln) 
                        if !parse_dependence(ln) 
                            parse_value(ln)
                        end
                    end
                end
            end
        end
    end
    
    def parse_functions
        gl_typemap = GLtypemap.new
        gl_typemap.parse
        
        file = File.open('./gl.spec').read
        file.gsub!(/\r\n?/, "\n")

        gl_func = {:category => nil, :function => nil}
        gl_type = Proc.new do |typename| 
            gl_typemap.types.has_key?(typename) ? gl_typemap.types[typename] : typename 
        end
        
        file.each_line do |line|
            if !line.start_with?("#") and !line.strip.empty?
                ln = line.split(" ")
                
                if !ln[0].end_with?(":")
                    #if (!line.index("(").nil?)
                    if !line.start_with?("\t")
                        # parse function name
                        n = line[0..line.index("(") - 1]
                        
                        gl_func[:function] = GLfunction.new(n)                        
                    else
                        # parse params
                        case ln[0]
                        when "return"
                            gl_func[:function].ret_type = gl_type.call(ln[1])
                        when "param"
                            tn = gl_type.call(ln[2])
                            
                            case ln[3]
                            when "in"
                                tn = "const " + tn + "*" if ln[4] == "array"
                            when "out"
                                tn = tn + "*"
                            end
                            
                            gl_func[:function].add_param(tn, ln[1])
                        when "category"
                            gl_func[:category] = ln[1]
                        end
                    end
                end
            end
            
            if line.strip.empty? 
                if !gl_func[:category].nil? and !gl_func[:function].nil?
                    @enums.each { |e| e.add_function(gl_func[:function]) if e.name == gl_func[:category] }
                end
                
                gl_func[:category] = nil
                gl_func[:function] = nil
            end
        end
    end
end

# first - download spec files
=begin
download_spec("gl.tm")
download_spec("enum.spec")
download_spec("enumext.spec")
download_spec("gl.spec")
=end
# next - parse gl type map



# parse extensions enums

ee = EnumExtSpec.new
ee.parse_enums
ee.parse_functions

#puts %-vendors => %{v}- % {:v => ee.vendors.join(", ")}


# finally - create well-formed c headers

tmp_dir = './gl'

Dir.mkdir(tmp_dir) if !Dir.exists?(tmp_dir)
Dir.chdir(tmp_dir)

File.open("./gl_types.h", "wt+") do |f|
    h = "GL_TYPES"
            
    f.puts "#ifndef %{h}" % {:h => h}
    f.puts "#define %{h} 1" % {:h => h}
    f.puts 
    f.puts "typedef unsigned int    GLenum;"
    f.puts "typedef unsigned char   GLboolean;"
    f.puts "typedef unsigned int    GLbitfield;"
    f.puts "typedef void            GLvoid;"
    f.puts "typedef signed char     GLbyte;     /* 1-byte signed */"
    f.puts "typedef short           GLshort;    /* 2-byte signed */"
    f.puts "typedef int             GLint;      /* 4-byte signed */"
    f.puts "typedef unsigned char   GLubyte;    /* 1-byte unsigned */"
    f.puts "typedef unsigned short  GLushort;   /* 2-byte unsigned */"
    f.puts "typedef unsigned int    GLuint;     /* 4-byte unsigned */"
    f.puts "typedef int             GLsizei;    /* 4-byte signed */"
    f.puts "typedef float           GLfloat;    /* single precision float */"
    f.puts "typedef float           GLclampf;   /* single precision float in [0,1] */"
    f.puts "typedef double          GLdouble;   /* double precision float */"
    f.puts "typedef double          GLclampd;   /* double precision float in [0,1] */"
    f.puts 
    f.puts "#endif // %{h}" % {:h => h}
end

ee.enums.each do |e|
    e.save(Dir.pwd)
end









